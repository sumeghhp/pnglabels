import cairo
#better import the csv with pandas
name = []
designation = []
address = []
max = len(name)
for i in range (0, max):
    with cairo.SVGSurface("teacher{}.svg".format(i+1), 500, 200) as surface:#i+1 to not confuse index with serial number
  
    # creating a cairo context object
        context = cairo.Context(surface)
        context.select_font_face("Raleway", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
        context.set_source_rgb(0.3, 0.45, 0.87)#picking a color
        context.rectangle(0, 0, 500, 200)#making a rectangle with given color
        context.fill()#filling the rectangle
        context.set_source_rgb(0,0,0)#resetting the color
        context.move_to(30, 60)#first line locaton
        context.set_font_size(25)
        context.show_text("{}".format(name[i]))
        context.move_to(30, 110)#second line location
        context.set_font_size(20)
        context.show_text("{}".format(designation[i]))
        context.move_to(30, 160)#third line location
        context.set_font_size(15)
        context.show_text("{}".format(address[i]))
    print("File Saved")